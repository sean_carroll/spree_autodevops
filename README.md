# Spree Autodevops

Autodevops test using the [Spree example](https://github.com/spree/spree).

## Installation steps

Setup app

```sh
rails new spree_autodevops --database=postgresql
cd spree_autodevops
```

Add to Gemfile

```ruby
gem 'spree', '~> 4.1'
gem 'spree_auth_devise', '~> 4.2'
gem 'spree_gateway', '~> 3.9'
```

Bundle

```sh
bundle install
bundle update
```

`bundle update` is done to fix an issue with Sprockets as detailled on the Spree homepage

Run Spree generators

```sh
bundle exec rails g spree:install --user_class=Spree::User
bundle exec rails g spree:auth:install
bundle exec rails g spree_gateway:install
```